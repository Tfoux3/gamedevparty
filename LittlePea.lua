LittlePea = {}
LittlePea.__index = LittlePea

function LittlePea:create(x,y,sprite,size)
  local lilPea = {}
  setmetatable(lilPea, self)
  lilPea._x = x
  lilPea._y = y
  lilPea._size = size
  if sprite then
    lilPea._sprite = love.graphics.newImage(sprite)
  end
  return lilPea
end

function LittlePea:Draw()
  if self._sprite  then
    love.graphics.draw(self._sprite, self._x, self._y)
  else
    love.graphics.circle("fill",self._x,self._y,50)
  end
end

function LittlePea:Move(x,y)
  self._x = self._x + x
  self._y =self._y + y
end


--
