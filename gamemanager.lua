require "LittlePea"
GameManager = {}
GameManager.__index = GameManager

function GameManager:new(tileSize)
  local o = {_player = nil, _labyrinth = nil, _tileSize= tileSize}
  setmetatable(o, self)
  return o
end

function GameManager:init()
  print("init")
  self._labyrinth = Labyrinth:load("C:\\Users\\bellecour\\Documents\\gamedevparty\\out.txt")

end

function GameManager:load()
  print("load")
  joysticks = love.joystick.getJoysticks()
  joystick = joysticks[1]
  x = 400
  y = 300
  pea = LittlePea:create(x,y,nil,25)
  speed = 300
end

function GameManager:update(dt)
  print("update")
  if not joystick then return end
  self:HandleAxis(joystick,dt,speed,pea)
  end

function GameManager:draw()
  print("draw")
  pea:Draw()
end



function GameManager:HandleAxis(j,dt,speed,pea)
  if math.abs(j:getGamepadAxis("leftx")) < 0.2 then
    xoffset = 0
  else
    xoffset = j:getGamepadAxis("leftx")
  end

  if math.abs(j:getGamepadAxis("lefty")) < 0.2 then
    yoffset = 0
  else
    yoffset = j:getGamepadAxis("lefty")
  end
  x,y = 0,0
  nextX = speed*xoffset* dt
  nextY = speed*yoffset * dt

  ob = self:getMyCorners(pea._x, speed*yoffset * dt,pea)
  --
  -- if yoffset <0 then
  --   if ob.upleft == 1 and ob.upright ==1 then
  --     y = nextY
  --   end
  -- else
  --   if ob.downleft == 1 and ob.downright ==1 then
  --     y = nextY
  --   end
  -- end
  --
  -- ob = getMyCorners(speed*yoffset * dt,pea._y,pea)
  -- if xoffset <0 then
  --   if ob.downleft == 1 and ob.upleft ==1 then
  --     x = nextX
  --   end
  -- else
  --   if ob.downright == 1 and ob.upright ==1 then
  --     x = nextX
  --   end
  -- end
end

-- function GameManager:getMyCorners (x, y,pea)
--   ob = {}
--   ob.downY = math.floor((y+pea._size-1)/self._tileSize)
--   ob.upY = math.floor((y-pea._size)/self._tileSize)
--   ob.leftX = math.floor((x-pea._size)/self._tileSize)
--   ob.rightX = math.floor((x+pea._size-1)/self._tileSize)
--
--   ob.upleft = self._labyrinth._tiles[ob.upY][ob.leftX]
--   -- :type()
--   -- ob.downleft = self._labyrinth._tiles[ob.downY][ob.leftX]:type()
--   -- ob.upright = self._labyrinth._tiles[ob.upY][ob.rightX]:type()
--   -- ob.downright = self._labyrinth._tiles[ob.downY][ob.rightX]:type()
--   return ob
-- end
